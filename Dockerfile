FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /app

# copy csproj and restore as distinct layers
#COPY *.sln .
COPY *.csproj .
RUN dotnet restore

# copy everything else and build app
COPY . ./razorpages/
WORKDIR /app/razorpages
RUN dotnet publish -c Release -o out


FROM microsoft/dotnet:2.2-aspnetcore-runtime AS runtime
WORKDIR /app
EXPOSE 1433
COPY --from=build /app/razorpages/out ./
ENTRYPOINT ["dotnet", "RazorPages.dll"]