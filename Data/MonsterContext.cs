﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RazorPages.Models;

namespace RazorPages.Models
{
    public class MonsterContext : DbContext
    {
        public MonsterContext (DbContextOptions<MonsterContext> options)
            : base(options)
        {
        }

        public DbSet<RazorPages.Models.Lair> Lair { get; set; }

        public DbSet<RazorPages.Models.Monster> Monster { get; set; }
    }
}
