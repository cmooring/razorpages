﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RazorPages.Models;
using System;
using System.Net;

namespace RazorPages
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //services.AddDbContext<MonsterContext>(options =>
            //        options.UseSqlServer(Configuration.GetConnectionString("MonsterContext")));

            services.AddDbContext<MonsterContext>(options =>
                        options.UseMySQL(Configuration.GetConnectionString("MonsterContextMySql")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            String DockerHostMachineIpAddress, DockerHostInternalIpAddress;
            try
            {
                DockerHostMachineIpAddress = Dns.GetHostAddresses(new Uri("http://docker.for.win.localhost").Host)[0].ToString();
            }
            catch (Exception ex)
            {
                DockerHostMachineIpAddress = ex.Message;
            }

            try
            {
                DockerHostInternalIpAddress = Dns.GetHostAddresses(new Uri("http://host.docker.internal").Host)[0].ToString();
            }
            catch (Exception ex)
            {
                DockerHostInternalIpAddress = ex.Message;
            }

            System.Diagnostics.Debug.WriteLine($"Address Info is: {DockerHostInternalIpAddress} and {DockerHostMachineIpAddress}");

            //  app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc();
        }
    }
}
