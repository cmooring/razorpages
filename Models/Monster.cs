using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RazorPages.Models
{
    public class Monster
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public decimal Price { get; set; }

        public ICollection<Lair> Haunts { get; set; }
    }
}