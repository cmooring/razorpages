using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RazorPages.Models
{
    public class Haunts
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        public int MonsterID { get; set; }
        public int LairID { get; set; }

        public Monster Monster { get; set; }
        public Lair Lair { get; set; }
        
    }
}