using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RazorPages.Models
{
    public class Lair
    {
        public enum LairLocation { NorthWest, NorthEast, SouthWest, SouthEast };

        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
    }
}