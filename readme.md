## Purpose

This is a repository to explore .NET core while seeing how it can be deployed to various cloud providers. I'd also like to get it establised to experiment with CI/CD for deployment to various environments too.

Bitbucket pipelines

- https://asp.net-hacker.rocks/2017/12/08/bitbucket-pipelines.html  

- https://confluence.atlassian.com/bitbucket/deploy-to-amazon-ecs-892623902.html  

AWS  

- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/dotnet-core-tutorial.html  

    - Required setup of UnxUtils to create the **source bundle**  
    - CSS/JS etc didn't deploy on the first attempt. Need to recurse directories on zip via `zip -r site.zip *` 

- http://docs.servicestack.net/deploy-netcore-docker-aws-ecs  
    - Haven't used this yet, but it looks interesting at any rate.  

- https://elanderson.net/2018/06/deploying-an-asp-net-core-application-to-amazon-web-services/  

    - Still need to look into this one more. Seems to have some good ideas on producing solution files etc, and then direct integration in Visual Studio  

- https://aws.amazon.com/cli/  

- https://dev.to/donbavand/deploying-a-dockerized-net-core-application-with-bitbucket-pipelines-and-aws-7dg  

    - Looks promising (step 2) for deploying application to container, prior to pushing to EC2 instance.  

AWS Architecture

- https://devblogs.microsoft.com/premier-developer/differentiating-between-azure-virtual-network-vnet-and-aws-virtual-private-cloud-vpc/

    - Good comparison of how a VPC in AWS compares to a VNet in Azure

Docker  
This was my first foray into using Docker. Given I only have Windows 7, I had to use [Docker Toolbox](https://docs.docker.com/toolbox/overview/). However, this worked really well for me. 

When logging into my host machine, I just need to start the "Docker Quickstart Terminal" which will allow me to check that status of the docker machine and containers. Some useful commands are shown below;

`docker-machine ls`  
Lists details of any docker machines

`docker ps -a`  
Lists any running docker containers. If exited, keep an eye on the exit code.

`docker build --pull -t razorpages .`
After changing to the directory that contains the project and related Dockerfile, this command will build the container.

`docker run -d -p 8080:80 --name rp razorpages`  
Run dockerized application listening on port 8080

`docker inspect rp`  
`docker inspect --format '{{ .NetworkSettings.IPAddress }}' rp`  '
A couple of commands to show the settings of a given container.

`docker-machine ip`  
Get the IP address allocated to the docker-machine. This is what I use with the browser in conjunction with the port specified in the above `docker run` command to be able to access the application via the browser.

`docker logs <container-name>`  
Show some log output from the given container.

`docker run -p 8080:80 -e ConnectionStrings:MonsterContext='Server=192.168.2.14\SQLEXPRESS,1433;Database=MonsterContext-c433f4a3-ebea-48e1-b12c-d412828a8e89;User Id=test;Password=test;MultipleActiveResultSets=true' razorpages`
To run docker and connect to a local instance of SQLExpress. Needed to [change dynamic port mapping with named instance](https://dba.stackexchange.com/a/62300/9936) to fixed port mapping

`docker ps -a | grep -i razorpages | grep Up | awk '{ print $1 }' | xargs docker stop`
To kill the current running instance of razorpages

I've added SSH authentication to this repository, so want to check if this works

