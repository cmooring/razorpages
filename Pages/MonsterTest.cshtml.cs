using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace RazorPages.Pages
{
    public class MonsterTestModel : PageModel
    {

        public string Name { get; set; }
        public void OnGet()
        {
            
            Name = RouteData.Values["name"] != null ? RouteData.Values["name"].ToString() : "";
            
        }
    }
}