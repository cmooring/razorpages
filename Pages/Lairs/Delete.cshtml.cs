﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RazorPages.Models;

namespace RazorPages.Pages.Lairs
{
    public class DeleteModel : PageModel
    {
        private readonly RazorPages.Models.MonsterContext _context;

        public DeleteModel(RazorPages.Models.MonsterContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Lair Lair { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Lair = await _context.Lair.FirstOrDefaultAsync(m => m.ID == id);

            if (Lair == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Lair = await _context.Lair.FindAsync(id);

            if (Lair != null)
            {
                _context.Lair.Remove(Lair);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
