﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RazorPages.Models;

namespace RazorPages.Pages.Lairs
{
    public class EditModel : PageModel
    {
        private readonly RazorPages.Models.MonsterContext _context;

        public EditModel(RazorPages.Models.MonsterContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Lair Lair { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Lair = await _context.Lair.FirstOrDefaultAsync(m => m.ID == id);

            if (Lair == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Lair).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LairExists(Lair.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool LairExists(int id)
        {
            return _context.Lair.Any(e => e.ID == id);
        }
    }
}
