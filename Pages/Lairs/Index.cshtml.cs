﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RazorPages.Models;

namespace RazorPages.Pages.Lairs
{
    public class IndexModel : PageModel
    {
        private readonly RazorPages.Models.MonsterContext _context;

        public IndexModel(RazorPages.Models.MonsterContext context)
        {
            _context = context;
        }

        public IList<Lair> Lair { get;set; }

        public async Task OnGetAsync()
        {
            Lair = await _context.Lair.ToListAsync();
        }
    }
}
