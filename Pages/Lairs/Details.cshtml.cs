﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RazorPages.Models;

namespace RazorPages.Pages.Lairs
{
    public class DetailsModel : PageModel
    {
        private readonly RazorPages.Models.MonsterContext _context;

        public DetailsModel(RazorPages.Models.MonsterContext context)
        {
            _context = context;
        }

        public Lair Lair { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Lair = await _context.Lair.FirstOrDefaultAsync(m => m.ID == id);

            if (Lair == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
